package com.fszuberski.twisual.data.entity;

import com.fszuberski.twisual.integration.entity.BitcoinPrice;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class BitcoinPriceData {

    @Id
    private String id;
    private Double high;
    private Double low;
    private Double open;
    private Double average;
    private String time;
    private Double volume;

    public BitcoinPriceData(BitcoinPrice bitcoinPrice) {
        this.high = bitcoinPrice.getHigh();
        this.low = bitcoinPrice.getLow();
        this.open = bitcoinPrice.getOpen();
        this.average = bitcoinPrice.getAverage();
        this.time = bitcoinPrice.getTime();
        this.volume = bitcoinPrice.getVolume();
    }
}
