package com.fszuberski.twisual.data.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import twitter4j.*;

import java.util.Date;

@Data
public class TwitterStatus {

    @Id
    private String id;
    private Date createdAt;
    private long twitter_id;
    private String text;
    private int displayTextRangeStart = -1;
    private int displayTextRangeEnd = -1;
    private String source;
    private boolean isTruncated;
    private long inReplyToStatusId;
    private long inReplyToUserId;
    private boolean isFavorited;
    private boolean isRetweeted;
    private int favoriteCount;
    private String inReplyToScreenName;
    private GeoLocation geoLocation = null;
    private Place place = null;
    private long retweetCount;
    private boolean isPossiblySensitive;
    private String lang;
    private long[] contributorsIDs;
    private Status retweetedStatus;
    private UserMentionEntity[] userMentionEntities;
    private URLEntity[] urlEntities;
    private HashtagEntity[] hashtagEntities;
    private MediaEntity[] mediaEntities;
    private SymbolEntity[] symbolEntities;
    private long currentUserRetweetId = -1L;
    private Scopes scopes;
    private User user = null;
    private String[] withheldInCountries = null;
    private Status quotedStatus;
    private long quotedStatusId = -1L;

    public TwitterStatus(Status status) {
        this.createdAt = status.getCreatedAt();
        this.twitter_id = status.getId();
        this.text = status.getText();
        this.displayTextRangeStart = status.getDisplayTextRangeStart();
        this.displayTextRangeEnd = status.getDisplayTextRangeEnd();
        this.source = status.getSource();
        this.isTruncated = status.isTruncated();
        this.inReplyToStatusId = status.getInReplyToStatusId();
        this.inReplyToUserId = status.getInReplyToUserId();
        this.isFavorited = status.isFavorited();
        this.isRetweeted = status.isRetweeted();
        this.favoriteCount = status.getFavoriteCount();
        this.inReplyToScreenName = status.getInReplyToScreenName();
        this.geoLocation = status.getGeoLocation();
        this.place = status.getPlace();
        this.retweetCount = status.getRetweetCount();
        this.isPossiblySensitive = status.isPossiblySensitive();
        this.lang = status.getLang();
        this.contributorsIDs = status.getContributors();
        this.retweetedStatus = status.getRetweetedStatus();
        this.userMentionEntities = status.getUserMentionEntities();
        this.urlEntities = status.getURLEntities();
        this.hashtagEntities = status.getHashtagEntities();
        this.mediaEntities = status.getMediaEntities();
        this.symbolEntities = status.getSymbolEntities();
        this.currentUserRetweetId = status.getCurrentUserRetweetId();
        this.scopes = status.getScopes();
        this.user = status.getUser();
        this.withheldInCountries = status.getWithheldInCountries();
        this.quotedStatus = status.getQuotedStatus();
        this.quotedStatusId = status.getQuotedStatusId();
    }
}
