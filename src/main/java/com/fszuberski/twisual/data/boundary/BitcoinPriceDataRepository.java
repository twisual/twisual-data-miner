package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.BitcoinPriceData;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BitcoinPriceDataRepository extends RxJava2CrudRepository<BitcoinPriceData, String> {
}
