package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.TwitterStatus;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterStatusRepository extends RxJava2CrudRepository<TwitterStatus, String> {

}
