package com.fszuberski.twisual.common.util;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import java.util.concurrent.TimeUnit;

public class RetryWithDelayFunction implements Function<Observable<? extends Throwable>, Observable<?>> {

    private final long maxRetries;
    private final int retryDelay;
    private final TimeUnit retryTimeUnit;
    private long retryCount;

    public RetryWithDelayFunction(final int retryDelay, final TimeUnit retryTimeUnit) {
        this(Long.MAX_VALUE, retryDelay, retryTimeUnit);
    }

    public RetryWithDelayFunction(final long maxRetries, final int retryDelay, final TimeUnit retryTimeUnit) {
        this.maxRetries = maxRetries;
        this.retryDelay = retryDelay;
        this.retryTimeUnit = retryTimeUnit;
        this.retryCount = 0;
    }

    @Override
    public Observable<?> apply(final Observable<? extends Throwable> attempts) {
        return attempts
                .flatMap((Function<Throwable, Observable<?>>) throwable -> {
                    if (++retryCount < maxRetries) {
                        return Observable.timer(retryDelay, retryTimeUnit);
                    }

                    return Observable.error(throwable);
                });
    }
}