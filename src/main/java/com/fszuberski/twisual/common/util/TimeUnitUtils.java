package com.fszuberski.twisual.common.util;

import java.util.concurrent.TimeUnit;

public class TimeUnitUtils {

    public static TimeUnit timeUnitFromString(String timeUnitString) {
        switch (timeUnitString.toUpperCase()) {
            case "MILLISECONDS":
                return TimeUnit.MILLISECONDS;
            case "SECONDS":
                return TimeUnit.SECONDS;
            case "MINUTES":
                return TimeUnit.MINUTES;
            case "HOURS":
                return TimeUnit.HOURS;
            case "DAYS":
                return TimeUnit.DAYS;
            default:
                return TimeUnit.SECONDS;
        }
    }
}
