package com.fszuberski.twisual.common;

import com.fszuberski.twisual.common.configuration.manager.ThreadPoolManager;
import com.fszuberski.twisual.common.metrics.TweetsPerSecondMetric;
import com.fszuberski.twisual.common.util.DateUtils;
import com.fszuberski.twisual.integration.boundary.BitcoinAverageStreamObserver;
import com.fszuberski.twisual.integration.boundary.TwitterStreamObserver;
import com.fszuberski.twisual.integration.control.BitcoinAverageClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class ApplicationContextListener {

    private ThreadPoolManager threadPoolManager;
    private TwitterStreamObserver twitterStreamObserver;
    private TweetsPerSecondMetric tweetsPerSecondMetric;
    private BitcoinAverageStreamObserver bitcoinAverageStreamObserver;

    @Autowired
    public void setThreadPoolManager(ThreadPoolManager threadPoolManager) {
        this.threadPoolManager = threadPoolManager;
    }

    @Autowired
    public void setTwitterStreamObserver(TwitterStreamObserver twitterStreamObserver) {
        this.twitterStreamObserver = twitterStreamObserver;
    }

    @Autowired
    public void setTweetsPerSecondMetric(TweetsPerSecondMetric tweetsPerSecondMetric) {
        this.tweetsPerSecondMetric = tweetsPerSecondMetric;
    }

    @Autowired
    public void setBitcoinAverageStreamObserver(BitcoinAverageStreamObserver bitcoinAverageStreamObserver) {
        this.bitcoinAverageStreamObserver = bitcoinAverageStreamObserver;
    }

    @EventListener
    protected void contextRefreshed(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("ContextRefreshed: {}", contextRefreshedEvent);

        Executors.newSingleThreadExecutor().submit(() -> {
            log.info("Creating stream subscriber thread pool");
            threadPoolManager.createTwitterStreamThreadPool();

            log.info("Initializing Twitter stream");
            twitterStreamObserver.initialize();

            log.info("Initializing TweetsPerSecondMetric");
            tweetsPerSecondMetric.initialize();

            log.info("Initialization finished");
        });

        bitcoinAverageStreamObserver.downloadBitcoinHistoricalData();
    }

    @EventListener
    protected void contextClosed(ContextClosedEvent contextClosedEvent) {
        log.info("ContextClosed: {}", contextClosedEvent);

        log.info("deinitializing TweetsPerSecondMetric");
        tweetsPerSecondMetric.deinitialize();

        log.info("Deinitializing to Twitter stream");
        twitterStreamObserver.deinitialize();

        log.info("Shutting down stream subscriber thread pool");
        threadPoolManager.shutdownPublicStreamSubscriberPool();

        log.info("Shutdown finished");
    }
}
