package com.fszuberski.twisual.common.configuration;

import com.fszuberski.twisual.common.configuration.manager.PropertiesManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.conf.ConfigurationBuilder;

@Slf4j
@Configuration
public class TwitterConfiguration {

    private PropertiesManager propertiesManager;

    @Autowired
    public void setPropertiesManager(PropertiesManager propertiesManager) {
        this.propertiesManager = propertiesManager;
    }

    @Bean
    twitter4j.conf.Configuration twitter4JStreamConfiguration() {
        log.info("Twitter4J Stream configuration initialized");
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        return configurationBuilder
                .setDebugEnabled(false)
                .setOAuthConsumerKey(propertiesManager.getTwitterConsumerKey())
                .setOAuthConsumerSecret(propertiesManager.getTwitterConsumerSecret())
                .setOAuthAccessToken(propertiesManager.getTwitterAccessToken())
                .setOAuthAccessTokenSecret(propertiesManager.getTwitterAccessSecret())
                .setHttpRetryCount(Short.MAX_VALUE)
                .setHttpRetryIntervalSeconds(5)
                .setDaemonEnabled(true)
                .setTweetModeExtended(true)
                .setIncludeMyRetweetEnabled(true)
                .setIncludeEmailEnabled(true)
                .build();
    }
}
