package com.fszuberski.twisual.common.configuration;

import com.fszuberski.twisual.common.configuration.manager.PropertiesManager;
import com.fszuberski.twisual.common.util.HashUtils;
import com.fszuberski.twisual.integration.control.BitcoinAverageAPI;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
public class BitcoinAverageRetrofitConfiguration {

    private PropertiesManager propertiesManager;

    @Autowired
    public void setPropertiesManager(PropertiesManager propertiesManager) {
        this.propertiesManager = propertiesManager;
    }

    @Bean
    OkHttpClient okHttpClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient
                .Builder()
                .addInterceptor(hmacAuthInterceptor())
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES);

        okHttpClientBuilder.addInterceptor(loggingInterceptor);

        return okHttpClientBuilder.build();
    }

    @Bean
    Retrofit retrofit() {
        return new Retrofit
                .Builder()
                .baseUrl("https://apiv2.bitcoinaverage.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClient())
                .build();
    }

    @Bean
    BitcoinAverageAPI bitcoinAverageAPI() {
        return retrofit().create(BitcoinAverageAPI.class);
    }

    private Interceptor hmacAuthInterceptor() {
        return chain -> {
            Request request = chain
                    .request()
                    .newBuilder()
//                    .addHeader("Accept", "application/json")
//                    .addHeader("X-signature", generateSignature())
                    .addHeader("X-Testing", "testing")
                    .build();

            return chain.proceed(request);
        };
    }

    private String generateSignature() {
        String payload = TimeUnit.MILLISECONDS.toSeconds(Instant.now().toEpochMilli()) + "." + propertiesManager.getBitcoinAveragePublicKey();
        String signature = payload + "." + HashUtils.hmacSha256(propertiesManager.getBitcoinAveragePrivateKey(), payload);

        log.info("Generated signature: {}", signature);
        return signature;
    }
}
