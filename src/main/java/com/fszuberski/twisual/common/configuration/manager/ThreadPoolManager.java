package com.fszuberski.twisual.common.configuration.manager;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import static com.fszuberski.twisual.common.util.TimeUnitUtils.timeUnitFromString;

@Slf4j
@Component
public class ThreadPoolManager {

    private PropertiesManager propertiesManager;

    @Getter
    private ThreadPoolTaskExecutor twitterStreamExecutor;

    @Autowired
    public ThreadPoolManager(PropertiesManager propertiesManager) {
        this.propertiesManager = propertiesManager;
    }

    public void createTwitterStreamThreadPool() {
        if (twitterStreamExecutor == null) {
            log.info("Creating Twitter stream thread pool");
            twitterStreamExecutor = new ThreadPoolTaskExecutor();
            twitterStreamExecutor.setCorePoolSize(propertiesManager.getTwitterStreamThreadPoolCorePoolSize());
            twitterStreamExecutor.setMaxPoolSize(propertiesManager.getTwitterStreamMaximumPoolSize());
            twitterStreamExecutor.setKeepAliveSeconds(
                    (int) timeUnitFromString(propertiesManager.getPublicStreamSubscriberKeepAliveTimeUnit())
                            .toSeconds(propertiesManager.getPublicStreamSubscriberKeepAliveTime()));
            twitterStreamExecutor.setQueueCapacity(propertiesManager.getPublicStreamSubscriberQueueCapacity());
            twitterStreamExecutor.setWaitForTasksToCompleteOnShutdown(true);
            twitterStreamExecutor.setAwaitTerminationSeconds(
                    (int) timeUnitFromString(propertiesManager.getPublicStreamSubscriberTimeoutTimeUnit())
                            .toSeconds(propertiesManager.getPublicStreamSubscriberTimeout()));
            twitterStreamExecutor.setThreadNamePrefix(propertiesManager.getPublicStreamSubscriberThreadNamePrefix());
            twitterStreamExecutor.setDaemon(true);
            twitterStreamExecutor.initialize();

            log.info("Twitter stream thread pool created - core pool size: {}, maximum pool size: {}, keep alive time: {} {}, " +
                            "blocking queue max capacity: {}, timeout: {} {}, thread name: {}",
                    propertiesManager.getTwitterStreamThreadPoolCorePoolSize(),
                    propertiesManager.getTwitterStreamMaximumPoolSize(),
                    propertiesManager.getPublicStreamSubscriberKeepAliveTime(),
                    propertiesManager.getPublicStreamSubscriberKeepAliveTimeUnit(),
                    propertiesManager.getPublicStreamSubscriberQueueCapacity(),
                    propertiesManager.getPublicStreamSubscriberTimeout(),
                    propertiesManager.getPublicStreamSubscriberTimeoutTimeUnit(),
                    propertiesManager.getPublicStreamSubscriberThreadNamePrefix());
        }
    }

    public void shutdownPublicStreamSubscriberPool() {
        if (twitterStreamExecutor != null) {
            log.info("Shutting down Twitter stream thread pool");
            twitterStreamExecutor.shutdown();
            twitterStreamExecutor = null;
            log.info("Shutdown of Twitter stream thread pool finished");
        }
    }
}
