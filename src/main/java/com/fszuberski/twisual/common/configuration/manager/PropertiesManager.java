package com.fszuberski.twisual.common.configuration.manager;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Component;

@Component
@PropertySources({
        @PropertySource(value = "classpath:application-default.properties"),
        @PropertySource(value = "classpath:application-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
})
public class PropertiesManager {

    //------------------------------------------------------------------------------------------------------------------ authentication

    @Getter
    @Value("${twitter.authentication.consumer.key}")
    private String twitterConsumerKey;

    @Getter
    @Value("${twitter.authentication.consumer.secret}")
    private String twitterConsumerSecret;

    @Getter
    @Value("${twitter.authentication.access.token}")
    private String twitterAccessToken;

    @Getter
    @Value("${twitter.authentication.access.secret}")
    private String twitterAccessSecret;

    @Getter
    @Value("${bitcoinaverage.authentication.public}")
    private String bitcoinAveragePublicKey;

    @Getter
    @Value("${bitcoinaverage.authentication.private}")
    private String bitcoinAveragePrivateKey;

    //------------------------------------------------------------------------------------------------------------------ thread pool

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.core-pool-size}")
    private Integer twitterStreamThreadPoolCorePoolSize;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.maximum-pool-size}")
    private Integer twitterStreamMaximumPoolSize;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.keep-alive-time}")
    private Integer publicStreamSubscriberKeepAliveTime;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.keep-alive-time-unit}")
    private String publicStreamSubscriberKeepAliveTimeUnit;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.queue-capacity}")
    private Integer publicStreamSubscriberQueueCapacity;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.timeout}")
    private Integer publicStreamSubscriberTimeout;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.timeout-time-unit}")
    private String publicStreamSubscriberTimeoutTimeUnit;

    @Getter
    @Value("${twisual.thread-pool.twitter-stream.thread-name-prefix}")
    private String publicStreamSubscriberThreadNamePrefix;
}
