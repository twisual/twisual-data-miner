package com.fszuberski.twisual.common.metrics;

import com.fszuberski.twisual.integration.boundary.TwitterStreamObserver;
import io.micrometer.core.instrument.Metrics;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class TweetsPerSecondMetric {

    private static AtomicInteger tweetsPerSecond = Metrics.gauge("twisual.tweets-per-second", new AtomicInteger(0));

    private TwitterStreamObserver twitterStreamObserver;

    private Disposable disposable;

    @Autowired
    public void setTwitterStreamObserver(TwitterStreamObserver twitterStreamObserver) {
        this.twitterStreamObserver = twitterStreamObserver;
    }

    public void initialize() {
        disposable = twitterStreamObserver
                .getObservable()
                .buffer(1, TimeUnit.SECONDS)
                .doOnNext(tweets -> tweetsPerSecond.getAndSet(tweets.size()))
                .subscribe();
    }

    public void deinitialize() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
