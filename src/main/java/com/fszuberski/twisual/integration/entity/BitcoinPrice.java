package com.fszuberski.twisual.integration.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BitcoinPrice {

    private Double high;
    private Double low;
    private Double open;
    private Double average;
    private String time;
    private Double volume;
}
