package com.fszuberski.twisual.integration.control;

import com.fszuberski.twisual.integration.entity.BitcoinPrice;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class BitcoinAverageClient {

    private Retrofit retrofit;

    @Autowired
    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public Observable<BitcoinPrice> getBitcoinPriceForDate(Date date) {
        log.info("::getBitcoinPriceForDate invoked with parameter: date={}", date);

        BitcoinAverageAPI bitcoinAverageAPI = retrofit.create(BitcoinAverageAPI.class);

        return bitcoinAverageAPI
                .bitcoinPriceAt(TimeUnit.MILLISECONDS.toSeconds(date.getTime()));
    }
}
