package com.fszuberski.twisual.integration.control;

import com.fszuberski.twisual.integration.entity.BitcoinPrice;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BitcoinAverageAPI {

    @GET("/indices/global/history/BTCUSD")
    Observable<BitcoinPrice> bitcoinPriceAt(@Query("at") Long timestamp);
}
