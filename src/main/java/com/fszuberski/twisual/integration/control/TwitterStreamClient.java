package com.fszuberski.twisual.integration.control;

import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.*;
import twitter4j.conf.Configuration;

@Slf4j
@Component
public class TwitterStreamClient {

    protected Configuration configuration;
    private TwitterStream twitterStream;

    @Autowired
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Observable<Status> create() {
        log.info("::create invoked");
        return Observable
                .create(subscriber -> {
                    twitterStream = new TwitterStreamFactory(configuration).getInstance();
                    twitterStream.addListener(new StatusAdapter() {

                        @Override
                        public void onStatus(Status status) {
                            subscriber.onNext(status);
                        }

                        @Override
                        public void onException(Exception e) {
                            log.warn("Exception while consuming Twitter Public Stream", e);
                        }

                        @Override
                        public void onStallWarning(StallWarning warning) {
                            log.warn("Stall warning: {}", warning);
                        }

                        @Override
                        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                            log.warn("Track limitation notice: {}", numberOfLimitedStatuses);
                        }
                    });
                    twitterStream.sample();
                });
    }

    public void shutdown() {
        log.info("::shutdown invoked");
        if (twitterStream != null) {
            log.info("Shutting down {}...", TwitterStream.class);
            twitterStream.shutdown();
            log.info("{} was shutdown successfully", TwitterStream.class);
        }
    }
}
