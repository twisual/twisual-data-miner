package com.fszuberski.twisual.integration.boundary;

import com.fszuberski.twisual.common.configuration.manager.ThreadPoolManager;
import com.fszuberski.twisual.common.util.RetryWithDelayFunction;
import com.fszuberski.twisual.data.boundary.TwitterStatusRepository;
import com.fszuberski.twisual.data.entity.TwitterStatus;
import com.fszuberski.twisual.integration.control.TwitterStreamClient;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class TwitterStreamObserver {

    private ThreadPoolManager threadPoolManager;
    private TwitterStreamClient twitterStreamClient;
    private TwitterStatusRepository twitterStatusRepository;

    @Getter
    private Observable<TwitterStatus> observable;
    private Disposable disposable;

    @Autowired
    public void setThreadPoolManager(ThreadPoolManager threadPoolManager) {
        this.threadPoolManager = threadPoolManager;
    }

    @Autowired
    public void setTwitterStreamClient(TwitterStreamClient twitterStreamClient) {
        this.twitterStreamClient = twitterStreamClient;
    }

    @Autowired
    public void setTwitterStatusRepository(TwitterStatusRepository twitterStatusRepository) {
        this.twitterStatusRepository = twitterStatusRepository;
    }

    public void initialize() {
        observable = twitterStreamClient
                .create()
                .flatMap(status -> twitterStatusRepository
                        .save(new TwitterStatus(status))
                        .toObservable()
                        .doOnError(e -> log.warn("Exception while saving {} to the database", TwitterStatus.class.getSimpleName(), e))
                        .subscribeOn(Schedulers.from(threadPoolManager.getTwitterStreamExecutor())))
                .doOnSubscribe(status -> log.info("::onSubscribe"))
                .subscribeOn(Schedulers.from(Executors.newSingleThreadExecutor()))
                .retryWhen(new RetryWithDelayFunction(100, TimeUnit.MILLISECONDS))
                .share();

        disposable = observable.subscribe(
                status -> {
                },
                e -> log.warn("Error while consuming TwitterPublicStream", e),
                () -> log.error("TwitterPublicStream completed - ")
        );
    }

    public void deinitialize() {
        if (observable != null) {
            twitterStreamClient.shutdown();

            if (disposable != null && !disposable.isDisposed()) {
                disposable.dispose();
            }
        }
    }
}
