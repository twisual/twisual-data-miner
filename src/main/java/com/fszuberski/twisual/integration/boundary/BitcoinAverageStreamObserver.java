package com.fszuberski.twisual.integration.boundary;

import com.fszuberski.twisual.data.boundary.BitcoinPriceDataRepository;
import com.fszuberski.twisual.data.entity.BitcoinPriceData;
import com.fszuberski.twisual.integration.control.BitcoinAverageClient;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static com.fszuberski.twisual.common.util.DateUtils.*;

@Slf4j
@Service
public class BitcoinAverageStreamObserver {

    private BitcoinAverageClient bitcoinAverageClient;
    private BitcoinPriceDataRepository bitcoinPriceDataRepository;

    @Autowired
    public void setBitcoinAverageClient(BitcoinAverageClient bitcoinAverageClient) {
        this.bitcoinAverageClient = bitcoinAverageClient;
    }

    @Autowired
    public void setBitcoinPriceDataRepository(BitcoinPriceDataRepository bitcoinPriceDataRepository) {
        this.bitcoinPriceDataRepository = bitcoinPriceDataRepository;
    }

    @Scheduled(cron = "${schedulers.bitcoinaverage.price-check.cron-expression}")
    public void downloadBitcoinPriceData() {
        bitcoinAverageClient
                .getBitcoinPriceForDate(yesterday())
                .flatMap(bitcoinPrice -> bitcoinPriceDataRepository
                        .save(new BitcoinPriceData(bitcoinPrice))
                        .toObservable())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }


    public void downloadBitcoinHistoricalData() {
        if (bitcoinPriceDataRepository.count().blockingGet() == 0) {
            log.info("Downloading historical BTC price data...");

            try {
                Observable
                        .fromIterable(generateDatesFrom(longFormat().parse("2017-07-01 00:00:00")))
                        .flatMap(date -> bitcoinAverageClient
                                .getBitcoinPriceForDate(date))
                        .flatMap(bitcoinPrice -> bitcoinPriceDataRepository
                                .save(new BitcoinPriceData(bitcoinPrice))
                                .toObservable())
                        .doOnError(e -> log.warn("Exception while downloading historical bitcoin price", e))
                        .subscribeOn(Schedulers.io())
                        .subscribe();

                log.info("Downloading historical BTC price data finished.");
            } catch (Exception e) {
                log.warn("Exception while retrieving bitcoin price", e);
            }
        }
    }
}
