package com.fszuberski.twisual.api;

import com.fszuberski.twisual.data.entity.TwitterStatus;
import com.fszuberski.twisual.integration.boundary.TwitterStreamObserver;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.Status;

@Slf4j
@RestController
public class TwitterStreamEndpoint {

    private TwitterStreamObserver twitterStreamObserver;

    @Autowired
    public void setTwitterStreamObserver(TwitterStreamObserver twitterStreamObserver) {
        this.twitterStreamObserver = twitterStreamObserver;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/tweet-stream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Observable<TwitterStatus> tweetStream() {
        log.info("Client connected to tweetSteam");
        return twitterStreamObserver.getObservable();
    }

}
