package com.fszuberski.twisual.common.util;

import org.junit.Test;

import static java.util.concurrent.TimeUnit.*;
import static org.junit.Assert.assertEquals;

public class TimeUnitUtilsTest {

    @Test
    public void fromStringMillisecondsTest() {
        assertEquals(MILLISECONDS, TimeUnitUtils.timeUnitFromString("MILLISECONDS"));
    }

    @Test
    public void fromStringMillisecondsLowercaseTest() {
        assertEquals(MILLISECONDS, TimeUnitUtils.timeUnitFromString("milliseconds"));
    }

    @Test
    public void fromStringSecondsTest() {
        assertEquals(SECONDS, TimeUnitUtils.timeUnitFromString("SECONDS"));
    }

    @Test
    public void fromStringSecondsLowercaseTest() {
        assertEquals(SECONDS, TimeUnitUtils.timeUnitFromString("seconds"));
    }

    @Test
    public void fromStringMinutesTest() {
        assertEquals(MINUTES, TimeUnitUtils.timeUnitFromString("MINUTES"));
    }

    @Test
    public void fromStringMinutesLowercaseTest() {
        assertEquals(MINUTES, TimeUnitUtils.timeUnitFromString("minutes"));
    }

    @Test
    public void fromStringHoursTest() {
        assertEquals(HOURS, TimeUnitUtils.timeUnitFromString("HOURS"));
    }

    @Test
    public void fromStringHoursLowercaseTest() {
        assertEquals(HOURS, TimeUnitUtils.timeUnitFromString("hours"));
    }

    @Test
    public void fromStringDaysTest() {
        assertEquals(DAYS, TimeUnitUtils.timeUnitFromString("DAYS"));
    }

    @Test
    public void fromStringDaysLowercaseTest() {
        assertEquals(DAYS, TimeUnitUtils.timeUnitFromString("days"));
    }

    @Test
    public void fromStringDefaultTest() {
        assertEquals(SECONDS, TimeUnitUtils.timeUnitFromString("test"));
    }

}