package com.fszuberski.twisual.common.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HashUtilsTest {

    @Test
    public void hmacSha256Test() {
        String testSecretKey = "abcdef123456";
        String testPayload = "1234.YzQxNGYyMGI1YzJjNDg3YThkOGU1MTgwZWNhYjY4ODI=";
        String expectedDigest = "a963aa66dbe4871ca17bbd64d5c1043d2f9204f19538b6e0f69c78f04adab9c8";

        String generatedDigest = HashUtils.hmacSha256(testSecretKey, testPayload);

        assertEquals(expectedDigest, generatedDigest);
    }
}