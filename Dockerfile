FROM frolvlad/alpine-oraclejdk8:slim
MAINTAINER Filip Szuberski "filip.szuberski@gmail.com"
VOLUME /var/log
ADD build/libs/twisual-data-miner.jar /root/twisual-data-miner.jar
ARG PROFILE
ENV JAVA_OPTS="-Xms64m -Xmx128m -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=$PROFILE"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /root/twisual-data-miner.jar" ]