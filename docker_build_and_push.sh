#!/usr/bin/env bash

APP_NAME=twisual-data-miner
APP_VERSION=2.2

gradle clean build -x test
docker build -t registry.gitlab.com/twisual/${APP_NAME}:${APP_VERSION} .
docker push registry.gitlab.com/twisual/${APP_NAME}:${APP_VERSION}